#!/bin/sh -e
#
# PartKeepr Docker startup script
#
# Copyright (C) 2022 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

PARTKEEPR_DIR="/opt/partkeepr"
HTML_DIR="/var/www/html"
PARTKEEPR_VERSION_FILE="src/PartKeepr/CoreBundle/PartKeeprVersion.php"
PARTKEEPR_PARAMETERS_FILE="app/config/parameters.php"
PARTKEEPR_PARAMETERS_SETUP_FILE="app/config/parameters_setup.php"
CONFIG_VARS="
AUTHENTICATION_PROVIDER:authentication_provider:str
CACHE__DOCTRINE:cache.doctrine:str
CACHE__DUNGLAS:cache.dunglas:bool
DATABASE_DRIVER:database_driver:str
DATABASE_HOST:database_host:str
DATABASE_NAME:database_name:str
DATABASE_PASSWORD:database_password:str
DATABASE_PORT:database_port:str
DATABASE_USER:database_user:str
FR3D_LDAP__DRIVER__ACCOUNTCANONICALFORM:fr3d_ldap.driver.accountCanonicalForm:str
FR3D_LDAP__DRIVER__ACCOUNTDOMAINNAME:fr3d_ldap.driver.accountDomainName:str
FR3D_LDAP__DRIVER__ACCOUNTDOMAINNAMESHORT:fr3d_ldap.driver.accountDomainNameShort:str
FR3D_LDAP__DRIVER__ACCOUNTFILTERFORMAT:fr3d_ldap.driver.accountFilterFormat:str
FR3D_LDAP__DRIVER__BASEDN:fr3d_ldap.driver.baseDn:str
FR3D_LDAP__DRIVER__BINDREQUIRESDN:fr3d_ldap.driver.bindRequiresDn:bool
FR3D_LDAP__DRIVER__HOST:fr3d_ldap.driver.host:str
FR3D_LDAP__DRIVER__OPTREFERRALS:fr3d_ldap.driver.optReferrals:str
FR3D_LDAP__DRIVER__PASSWORD:fr3d_ldap.driver.password:str
FR3D_LDAP__DRIVER__PORT:fr3d_ldap.driver.port:str
FR3D_LDAP__DRIVER__USERNAME:fr3d_ldap.driver.username:str
FR3D_LDAP__DRIVER__USESSL:fr3d_ldap.driver.useSsl:str
FR3D_LDAP__DRIVER__USESTARTTLS:fr3d_ldap.driver.useStartTls:str
FR3D_LDAP__USER__ATTRIBUTE__EMAIL:fr3d_ldap.user.attribute.email:str
FR3D_LDAP__USER__ATTRIBUTE__USERNAME:fr3d_ldap.user.attribute.username:str
FR3D_LDAP__USER__BASEDN:fr3d_ldap.user.baseDn:str
FR3D_LDAP__USER__ENABLED:fr3d_ldap.user.enabled:bool
FR3D_LDAP__USER__FILTER:fr3d_ldap.user.filter:str
LOCALE:locale:str
MAILER_AUTH_MODE:mailer_auth_mode:str
MAILER_ENCRYPTION:mailer_encryption:str
MAILER_HOST:mailer_host:str
MAILER_PASSWORD:mailer_password:str
MAILER_PORT:mailer_port:str
MAILER_TRANSPORT:mailer_transport:str
MAILER_USER:mailer_user:str
PARTKEEPR__CATEGORY__PATH_SEPARATOR:partkeepr.category.path_separator:str
PARTKEEPR__CRONJOB__CHECK:partkeepr.cronjob.check:bool
PARTKEEPR__FILESYSTEM__DATA_DIRECTORY:partkeepr.filesystem.data_directory:str
PARTKEEPR__FILESYSTEM__QUOTA:partkeepr.filesystem.quota:bool
PARTKEEPR__FRONTEND__AUTO_LOGIN__ENABLED:partkeepr.frontend.auto_login.enabled:bool
PARTKEEPR__FRONTEND__AUTO_LOGIN__PASSWORD:partkeepr.frontend.auto_login.password:str
PARTKEEPR__FRONTEND__AUTO_LOGIN__USERNAME:partkeepr.frontend.auto_login.username:str
PARTKEEPR__FRONTEND__BASE_URL:partkeepr.frontend.base_url:bool
PARTKEEPR__FRONTEND__MOTD:partkeepr.frontend.motd:bool
PARTKEEPR__MAINTENANCE:partkeepr.maintenance:bool
PARTKEEPR__MAINTENANCE__MESSAGE:partkeepr.maintenance.message:str
PARTKEEPR__MAINTENANCE__TITLE:partkeepr.maintenance.title:str
PARTKEEPR__OCTOPART__APIKEY:partkeepr.octopart.apikey:str
PARTKEEPR__PARTS__INTERNALPARTNUMBERUNIQUE:partkeepr.parts.internalpartnumberunique:bool
PARTKEEPR__PARTS__LIMIT:partkeepr.parts.limit:bool
PARTKEEPR__PATREON__STATUSURI:partkeepr.patreon.statusuri:str
PARTKEEPR__UPLOAD__LIMIT:partkeepr.upload.limit:bool
PARTKEEPR__USERS__LIMIT:partkeepr.users.limit:bool
SECRET:secret:str
"

setup_config() {
	unset removed_params
	for config_var in $1; do
		_config_var="${config_var%:*}"
		_parameter="${_config_var##*:}"
		_var="${_config_var%:*}"
		_value=$(eval "echo \$$_var")
		if [ -n "$_value" ]; then
			removed_params="${removed_params:+$removed_params\|}$_parameter"
		fi
	done
	removed_params="$(echo "$removed_params" | sed 's/\./\\./g')"
	grep -v -e "^<?php" -e "^\$container->setParameter('\($removed_params\)',"
}

vars_config() {
	for config_var in $1; do
		_type="${config_var##*:}"
		_config_var="${config_var%:*}"
		_parameter="${_config_var##*:}"
		_var="${_config_var%:*}"
		_value=$(eval "echo \$$_var")
		if [ -n "$_value" ]; then
			case $_type in
				str)
					echo "\$container->setParameter('${_parameter}', '${_value}');"
					;;
				int|bool)
					echo "\$container->setParameter('${_parameter}', ${_value});"
					;;
			esac
		fi
	done
}

create_config() {
	echo "<?php"
	(
		if [ -f "$HTML_DIR/$PARTKEEPR_PARAMETERS_SETUP_FILE" ]; then
			setup_config "$1" < "$HTML_DIR/$PARTKEEPR_PARAMETERS_SETUP_FILE"
		fi
		vars_config "$1"
	) | sort
}

prepare() {
	if ! cmp -s "$PARTKEEPR_DIR/$PARTKEEPR_VERSION_FILE" "$HTML_DIR/$PARTKEEPR_VERSION_FILE"; then
		rsync -rl --delete \
		      --exclude "/$PARTKEEPR_PARAMETERS_SETUP_FILE" \
		      --exclude "/data" \
		      --exclude "/logs" \
		      "$PARTKEEPR_DIR/" "$HTML_DIR/"
		mkdir -p "$HTML_DIR/data"
		chown -R www-data:www-data "$HTML_DIR"
	fi
}

run() {
	prepare
	create_config "$CONFIG_VARS" > "$HTML_DIR/$PARTKEEPR_PARAMETERS_FILE"
	exec docker-php-entrypoint php-fpm \
	     -d date.timezone="$TZ" \
	     -d max_execution_time=120 \
	     -d upload_max_filesize=8M
}

if [ "$1" = "partkeepr" ]; then
	run
fi

exec docker-php-entrypoint "$@"
